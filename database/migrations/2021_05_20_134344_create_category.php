<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create ('categories', function (Blueprint $table) {
            $table->id ();
            $table->unsignedBigInteger ('pid')->default (0)->comment ('父ID');
            $table->unsignedBigInteger ('cover_id')->default (0)->comment ('分类图片');
            $table->tinyInteger ('module_id')->default (0)->comment ('分类模块');
            //$table->string ('module_id',100)->default ('')->comment ('分类标识[about,news,product,jobs]');
            $table->string ('cate_name', 20)->default ('')->comment ('分类名称');
            $table->string ('cate_desc', 200)->default ('')->comment ('分类描述');
            $table->smallInteger ('sort')->default (0)->comment ('排序');
            $table->tinyInteger ('status')->default (0)->comment ('状态【1=显示、4=隐藏】');
            $table->timestamps ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists ('categories');
    }
}
