<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebMenus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create ('web_menus', function (Blueprint $table) {
            $table->id ();
            $table->unsignedBigInteger ('pid')->default (0)->comment ('父ID');
            $table->string ('name', 50)->default ('')->comment ('菜单标识');
            $table->string ('title', 100)->default ('')->comment ('菜单标题');
            $table->string ('seo_keyword', 150)->default ('')->comment ('SEO关键词');
            $table->string ('seo_description', 200)->default ('')->comment ('SEO描述');
            $table->string ('url', 200)->default ('')->comment ('地址');
            $table->string ('target', 10)->default ('')->comment ('链接类型');
            //$table->unsignedBigInteger ('category_id')->default (0)->comment ('采用分类ID');
            //$table->unsignedBigInteger ('page_id')->default (0)->comment ('采用页面ID');
            $table->morphs ('adopt');
            $table->smallInteger ('sort')->default (0)->comment ('排序');
            $table->tinyInteger ('status')->default (0)->comment ('状态');
            $table->timestamps ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists ('web_menus');
    }
}
