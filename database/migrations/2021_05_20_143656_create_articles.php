<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create ('articles', function (Blueprint $table) {
            $table->id ();
            $table->foreignId ('user_id')->constrained ()->cascadeOnDelete ();
            $table->foreignId ('menu_id')->constrained ()->cascadeOnDelete ();
            $table->foreignId ('category_id')->constrained ()->cascadeOnDelete ();
            $table->string ('name', 100)->nullable ()->unique ()->comment ('名称唯一标识');
            $table->string ('title', 200)->default ('')->comment ('标题');
            $table->unsignedBigInteger ('cover_id')->default (0)->comment ('封面图片');
            $table->text ('content')->comment ('内容');
            $table->unsignedBigInteger ('views_number')->default (0)->comment ('浏览次数');
            $table->string ('source', 100)->default ('')->comment ('来源');
            $table->timestamp ('published_at')->nullable ()->comment ('发表时间');
            $table->tinyInteger ('is_top')->default (0)->comment ('是否置顶');
            $table->tinyInteger ('status')->default (0)->comment ('状态【1=正常、4=隐藏】');
            $table->timestamps ();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists ('articles');
    }
}
